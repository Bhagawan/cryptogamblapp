package com.example.cryptogamblapp.ui.screens.mainMenuScreen

import androidx.lifecycle.ViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow

class MainMenuViewModel: ViewModel() {
    private val _addMoneyPopup = MutableStateFlow(false)
    val addMoneyPopup = _addMoneyPopup.asStateFlow()

    fun showPopup() {
        _addMoneyPopup.tryEmit(true)
    }

    fun hidePopup() {
        _addMoneyPopup.tryEmit(false)
    }
}