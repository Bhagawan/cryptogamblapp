package com.example.cryptogamblapp

import android.annotation.SuppressLint
import android.content.pm.ActivityInfo
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.telephony.TelephonyManager
import android.view.View
import android.webkit.WebChromeClient
import android.widget.FrameLayout
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.WindowInsetsControllerCompat
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.compose.rememberNavController
import com.example.cryptogamblapp.ui.theme.CryptoGamblAppTheme
import com.example.cryptogamblapp.util.CurrentAppData
import com.example.cryptogamblapp.util.NavigationComponent
import com.example.cryptogamblapp.util.SavedPrefs
import im.delight.android.webview.AdvancedWebView

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val viewModel = ViewModelProvider(this)[CryptoGamblMainViewModel::class.java]
        if(savedInstanceState == null) viewModel.init((getSystemService(TELEPHONY_SERVICE) as TelephonyManager).simCountryIso, SavedPrefs.getId(this))
        CurrentAppData.totalBalance = SavedPrefs.getBalance(this)
        setContent {
            val webView = remember { createWebView() }
            val navController = rememberNavController()
            CryptoGamblAppTheme() {
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    NavigationComponent(navController, webView, ::lockScreen)
                }
            }
        }
    }

    @SuppressLint("SourceLockedOrientationActivity")
    private fun lockScreen(lock: Boolean) {
        if(lock) requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
    }

    private fun createWebView() : AdvancedWebView {
        return AdvancedWebView(this).apply {
            setMixedContentAllowed(true)
            webChromeClient = object: WebChromeClient() {
                private var h = 0
                private var uiVisibility = 0
                private var d: View? = null
                private var a: CustomViewCallback? = null
                override fun getDefaultVideoPoster(): Bitmap? = if(d == null) null else BitmapFactory.decodeResource(resources, R.drawable.ic_baseline_video)
                override fun onHideCustomView() {
                    (window.decorView as FrameLayout).removeView(d)
                    d = null
                    window.decorView.systemUiVisibility = uiVisibility
                    requestedOrientation = h
                    a!!.onCustomViewHidden()
                    a = null
                }

                override fun onShowCustomView(view: View?, callback: CustomViewCallback?) {
                    if(d != null) {
                        onHideCustomView()
                        return
                    }
                    d = view
                    uiVisibility = window.decorView.systemUiVisibility
                    h = requestedOrientation
                    a = callback
                    (window.decorView as FrameLayout).addView(d, FrameLayout.LayoutParams(
                        FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT))
                    WindowInsetsControllerCompat(window, window.decorView).let { controller ->
                        controller.hide(WindowInsetsCompat.Type.systemBars())
                        controller.systemBarsBehavior = WindowInsetsControllerCompat.BEHAVIOR_SHOW_TRANSIENT_BARS_BY_SWIPE
                    }
                }
            }
            settings.userAgentString = settings.userAgentString.replace("; wv", "")
        }
    }
}