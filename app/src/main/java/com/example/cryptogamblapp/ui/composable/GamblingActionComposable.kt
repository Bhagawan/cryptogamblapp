package com.example.cryptogamblapp.ui.composable

import androidx.compose.foundation.border
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.cryptogamblapp.R
import com.example.cryptogamblapp.ui.theme.Green
import com.example.cryptogamblapp.ui.theme.Red

@Composable
fun GamblingActionComposable(modifier: Modifier = Modifier, gamePaused: Boolean = true, questionVisible: Boolean = false, onAction: (Boolean) -> Unit, onPause: (pause: Boolean) -> Unit) {
    val gameState = remember { mutableStateOf(gamePaused) }
    Column(
        modifier = modifier
            .fillMaxSize()
            .padding(5.dp)
            .border(width = 2.dp, color = Color.White, shape = RoundedCornerShape(10.dp))
            .padding(10.dp),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        if(!questionVisible) {
            Box(
                modifier = Modifier
                    .fillMaxWidth()
                    .weight(1.0f, true)
                    .padding(10.dp), contentAlignment = Alignment.Center
            ) {
                TextButton(onClick = {
                    gameState.value = !gameState.value
                    onPause(gameState.value)
                },
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(10.dp),
                    shape = RoundedCornerShape(5.dp),
                    colors = ButtonDefaults.buttonColors(containerColor = if(gameState.value) Green else Red,
                        contentColor = Color.White)) {
                    val string = if(gameState.value) stringResource(id = R.string.btn_start)
                    else stringResource(id = R.string.btn_pause)
                    Text(
                        string,
                        fontSize = 30.0f.sp,
                        color = Color.White,
                        textAlign = TextAlign.Center,
                        modifier = Modifier.weight(1.0f,true))
                }
            }
        } else {
            Button(onClick = { onAction(true) },
                modifier = Modifier
                    .fillMaxWidth()
                    .weight(1.0f, true)
                    .padding(10.dp),
                shape = RoundedCornerShape(5.dp),
                colors = ButtonDefaults.buttonColors(containerColor = Green,
                    contentColor = Color.White)) {
                Icon(painterResource(id = R.drawable.ic_baseline_trending_up_24), contentDescription = null, modifier = Modifier.fillMaxSize())
            }
            Button(onClick = { onAction(false) },
                modifier = Modifier
                    .fillMaxWidth()
                    .weight(1.0f, true)
                    .padding(10.dp),
                shape = RoundedCornerShape(5.dp),
                colors = ButtonDefaults.buttonColors(containerColor = Red,
                    contentColor = Color.White)) {
                Icon(painterResource(id = R.drawable.ic_baseline_trending_down_24), contentDescription = null, modifier = Modifier.fillMaxSize())
            }
        }
    }

}