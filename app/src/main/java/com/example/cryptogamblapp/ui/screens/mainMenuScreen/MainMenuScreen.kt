package com.example.cryptogamblapp.ui.screens.mainMenuScreen

import androidx.compose.foundation.Image
import androidx.compose.foundation.ScrollState
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.FilledIconButton
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButtonDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.TextUnitType
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import coil.compose.rememberImagePainter
import com.example.cryptogamblapp.R
import com.example.cryptogamblapp.ui.composable.AddMoneyPopup
import com.example.cryptogamblapp.ui.composable.CurrencyButton
import com.example.cryptogamblapp.ui.theme.Blue_light
import com.example.cryptogamblapp.util.Currency
import com.example.cryptogamblapp.util.CurrentAppData
import com.example.cryptogamblapp.util.SavedPrefs
import com.example.cryptogamblapp.util.UrlBack

@Composable
fun MainMenuScreen() {
    val viewModel =  viewModel(MainMenuViewModel::class.java)
    var balance by remember { mutableStateOf(CurrentAppData.totalBalance) }
    val context = LocalContext.current

    Image(painter = rememberImagePainter(UrlBack), contentDescription = "", modifier = Modifier.fillMaxSize(), contentScale = ContentScale.FillBounds)

    Column(
        modifier = Modifier
            .fillMaxSize(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally,

        ) {
        Text(text = stringResource(id = R.string.balance), fontSize = TextUnit(25.0f, TextUnitType.Sp), textAlign = TextAlign.Center, color = Color.White,
            modifier = Modifier.padding(horizontal = Dp(50.0f), vertical = Dp(10.0f)))
        Text(text = balance.toString(), fontSize = TextUnit(50.0f, TextUnitType.Sp), textAlign = TextAlign.Center, color = Color.White,
            modifier = Modifier.padding(horizontal = Dp(50.0f), vertical = Dp(10.0f)))

        FilledIconButton(onClick = { viewModel.showPopup() }, shape = CircleShape, colors = IconButtonDefaults.iconButtonColors(containerColor = Blue_light, contentColor = Color.White),
            modifier = Modifier
                .padding(20.dp)
                .align(Alignment.Start)) {
            Icon(painterResource(id = R.drawable.ic_baseline_add_circle_outline_24) , contentDescription = "")
        }
        val h = 100.0f
        Column(modifier = Modifier
            .fillMaxSize()
            .padding(start = 10.dp, end = 10.dp, top = 10.dp, bottom = 20.dp)
            .verticalScroll(
                ScrollState(0), enabled = true
            )) {
            for(currency in Currency.values()) {
                Box(modifier = Modifier
                    .fillMaxWidth()
                    .height(h.dp)
                    .padding(vertical = 10.dp)) {
                    CurrencyButton(currency = currency)
                }
            }
        }
    }

    val addMoneyPopup by viewModel.addMoneyPopup.collectAsState(false)

    if(addMoneyPopup) AddMoneyPopup {
        balance = CurrentAppData.totalBalance
        SavedPrefs.saveBalance(context)
        viewModel.hidePopup()
    }
}