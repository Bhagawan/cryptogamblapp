package com.example.cryptogamblapp.util

import androidx.activity.compose.BackHandler
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import com.example.cryptogamblapp.ui.screens.Screens
import com.example.cryptogamblapp.ui.screens.SplashScreen
import com.example.cryptogamblapp.ui.screens.WebViewScreen
import com.example.cryptogamblapp.ui.screens.gamblingScreen.GamblingScreen
import com.example.cryptogamblapp.ui.screens.mainMenuScreen.MainMenuScreen
import im.delight.android.webview.AdvancedWebView
import kotlin.system.exitProcess

@Composable
fun NavigationComponent(navController: NavHostController, webView: AdvancedWebView, orientationLock: (l: Boolean) -> Unit) {
    NavHost(
        navController = navController,
        startDestination = Screens.SPLASH_SCREEN.label
    ) {
        composable(Screens.SPLASH_SCREEN.label) {
            SplashScreen()
        }
        composable(Screens.WEB_VIEW.label) {
            BackHandler(true) {
                if (webView.onBackPressed()) exitProcess(0)
            }
            WebViewScreen(webView, remember { CurrentAppData.url })
        }
        composable(Screens.MAIN_SCREEN.label) {
            BackHandler(true) {}
            MainMenuScreen()
        }
        composable(Screens.GAMBLING_SCREEN.label) {
            BackHandler(true) {}
            GamblingScreen(CurrentAppData.chosenCurrency)
        }
    }
    val currentScreen by Navigator.navigationFlow.collectAsState(initial = Screens.SPLASH_SCREEN)
    if(currentScreen == Screens.MAIN_SCREEN) orientationLock(true)
    navController.navigate(currentScreen.label)

    navController.enableOnBackPressed(true)
}