package com.example.cryptogamblapp.util

import androidx.annotation.Keep

@Keep
data class CryptoGamblSplashResponse(val url : String)