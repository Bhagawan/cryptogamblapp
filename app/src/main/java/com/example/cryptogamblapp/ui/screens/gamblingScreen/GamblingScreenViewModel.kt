package com.example.cryptogamblapp.ui.screens.gamblingScreen

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.cryptogamblapp.util.CurrentAppData
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import kotlin.math.absoluteValue

class GamblingScreenViewModel: ViewModel() {
    private val _investedAmount = MutableStateFlow(0)
    val investedAmount = _investedAmount.asStateFlow()

    private val _investPopup = MutableStateFlow(false)
    val investPopup = _investPopup.asStateFlow()

    private val _question = MutableStateFlow(false)
    val question = _question.asStateFlow()

    private val _result = MutableStateFlow<Boolean?>(null)
    val result : StateFlow<Boolean?> = _result.asStateFlow()

    private val _graphActive = MutableSharedFlow<Boolean>(1)
    val graphActive = _graphActive.asSharedFlow()

    private var expectedDiff = 0.0f
    private var lastInvestmentChange = "+0.0%"

    fun showPopup() {
        _investPopup.tryEmit(true)
    }

    fun hidePopup() {
        _investPopup.tryEmit(false)
    }

    fun askQuestion(diff: Float) {
        expectedDiff = diff
        _question.tryEmit(true)
        viewModelScope.launch {
            _graphActive.emit(false)
        }
    }

    fun makeAGuess(up: Boolean) {
        _question.tryEmit(false)
        val s = if(up) expectedDiff >= 0 else expectedDiff < 0
        _result.tryEmit(s)
        val change = (investedAmount.value.absoluteValue / 100.0f * expectedDiff.absoluteValue).toInt()
        lastInvestmentChange = "${if(s) '+' else '-'}${expectedDiff.toInt().absoluteValue}%"
        if(s) _investedAmount.tryEmit(investedAmount.value + change)
        else _investedAmount.tryEmit(investedAmount.value - change)
    }

    fun hideResult() {
        _result.tryEmit(null)
        viewModelScope.launch {
            _graphActive.emit(true)
        }
    }

    fun buyCurrency(amount: Int) {
        _investPopup.tryEmit(false)
        val a = amount.coerceAtMost(CurrentAppData.totalBalance).coerceAtLeast(0)
        CurrentAppData.totalBalance -= a
        _investedAmount.tryEmit(investedAmount.value + a)
    }

    fun pauseGraph(pause: Boolean) {
        viewModelScope.launch {
            _graphActive.emit(!pause)
        }
    }

    fun getLastBalanceChange(): String = lastInvestmentChange

}