package com.example.cryptogamblapp.ui.composable

import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.cryptogamblapp.ui.theme.Green

@Composable
fun AddFixedAmountButton(amount: Int, modifier: Modifier = Modifier, onClicked: () -> Unit) {
    TextButton(onClick = { onClicked() },
        modifier = modifier,
        colors = ButtonDefaults.buttonColors(contentColor = Color.White, containerColor = Green),
        shape = RoundedCornerShape(5.0f.dp),
        elevation = ButtonDefaults.buttonElevation(defaultElevation = 1.0f.dp, pressedElevation = 0.0f.dp)) {
        Text(
            text = "+$amount",
            color = Color.White,
            fontSize = 15.sp,
            textAlign = TextAlign.Center,
            modifier = Modifier
                .padding(10.dp)
        )
    }
}