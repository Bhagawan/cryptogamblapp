package com.example.cryptogamblapp.ui.composable

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.cryptogamblapp.R
import com.example.cryptogamblapp.ui.theme.Gold
import com.example.cryptogamblapp.ui.theme.Grey_transparent
import com.example.cryptogamblapp.ui.theme.Grey_transparent_dark
import com.example.cryptogamblapp.ui.theme.Red
import com.example.cryptogamblapp.util.CurrentAppData

@Preview
@Composable
fun AddMoneyPopup(onFinish: () -> Unit = {}) {
    var money by remember { mutableStateOf(CurrentAppData.totalBalance)}
    Box(modifier = Modifier
        .fillMaxSize()
        .clickable { onFinish() }
        .background(color = Grey_transparent)
        .padding(horizontal = 20.dp), contentAlignment = Alignment.Center) {
        Column(
            modifier = Modifier
                .wrapContentSize()
                .background(color = Grey_transparent_dark, shape = RoundedCornerShape(20.0f.dp))
                .padding(10.0f.dp), horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Text(text = money.toString(),
                color = Gold,
                fontSize = 30.sp,
                textAlign = TextAlign.Center,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(10.dp))
            AmountTextField(modifier = Modifier
                .fillMaxWidth()
                .padding(3.dp)) {
                money += it
                CurrentAppData.totalBalance += it
            }
            Row(modifier = Modifier
                .fillMaxWidth()
                .padding(3.dp)) {
                AddFixedAmountButton(amount = 10, modifier = Modifier
                    .weight(1.0f, true)
                    .padding(10.0f.dp)) {
                    money += 10
                    CurrentAppData.totalBalance += 10
                }
                AddFixedAmountButton(amount = 100, modifier = Modifier
                    .weight(1.0f, true)
                    .padding(10.0f.dp)) {
                    money += 100
                    CurrentAppData.totalBalance += 100
                }
                AddFixedAmountButton(amount = 1000, modifier = Modifier
                    .weight(1.0f, true)
                    .padding(10.0f.dp)) {
                    money += 1000
                    CurrentAppData.totalBalance += 1000
                }
            }
            Button(onClick = { onFinish() },
                shape = RoundedCornerShape(5.0f.dp),
                modifier = Modifier.padding(10.0f.dp).fillMaxWidth(),
                colors = ButtonDefaults.buttonColors(contentColor = Color.White, containerColor = Red)) {
                Text(text = stringResource(id = R.string.desc_close),
                    color = Color.White,
                    fontSize = 20.sp,
                    textAlign = TextAlign.Center,
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(10.dp))
            }
        }
    }
}