package com.example.cryptogamblapp.ui.composable

import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.cryptogamblapp.ui.screens.Screens
import com.example.cryptogamblapp.ui.theme.Blue_light
import com.example.cryptogamblapp.util.Currency
import com.example.cryptogamblapp.util.CurrentAppData
import com.example.cryptogamblapp.util.Navigator

@Composable
fun CurrencyButton(currency: Currency) {
    TextButton(onClick = {
        CurrentAppData.chosenCurrency = currency
        Navigator.navigateTo(Screens.GAMBLING_SCREEN)
    },
        modifier = Modifier.fillMaxSize(),
        shape = RoundedCornerShape(5.dp),
        colors = ButtonDefaults.buttonColors(containerColor = Blue_light,
        contentColor = Color.White)) {
        Text(text = currency.name, textAlign = TextAlign.Start, color = Color.White, fontSize = 30.sp, modifier = Modifier.fillMaxWidth())
    }
}