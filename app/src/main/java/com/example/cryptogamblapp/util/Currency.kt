package com.example.cryptogamblapp.util

enum class Currency {
    Bitcoin,
    Ethereum,
    Cronos,
    Polygon
}