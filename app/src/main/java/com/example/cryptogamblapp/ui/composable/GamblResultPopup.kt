package com.example.cryptogamblapp.ui.composable

import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.cryptogamblapp.R
import com.example.cryptogamblapp.ui.theme.*

@Composable
fun GamblResultPopup(result: Boolean, amount: String, onClose: () -> Unit = {}) {
    Box(modifier = Modifier
        .fillMaxSize()
        .clickable { onClose() }
        .background(color = Grey_transparent)
        .padding(horizontal = 20.dp), contentAlignment = Alignment.Center) {
        Column(
            modifier = Modifier
                .wrapContentSize()
                .background(color = Grey_transparent_dark, shape = RoundedCornerShape(20.0f.dp))
                .padding(5.0f.dp)
                .border(shape = RoundedCornerShape(20.0f.dp), color = if(result) Gold else Red, width = 2.dp)
                .padding(10.dp), horizontalAlignment = Alignment.CenterHorizontally
        ) {
            val text = if(result) stringResource(id = R.string.success) else stringResource(id = R.string.failure)
            Text(text,
                color = if(result) Gold else Red,
                fontSize = 40.sp,
                textAlign = TextAlign.Center,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(10.dp))
            Text(amount,
                color = if(result) Green else Red,
                fontSize = 30.sp,
                textAlign = TextAlign.Center,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(10.dp))
        }
    }
}