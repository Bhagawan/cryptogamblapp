package com.example.cryptogamblapp.ui.theme

import androidx.compose.ui.graphics.Color

val Purple80 = Color(0xFFD0BCFF)
val PurpleGrey80 = Color(0xFFCCC2DC)
val Pink80 = Color(0xFFEFB8C8)

val Purple40 = Color(0xFF6650a4)
val PurpleGrey40 = Color(0xFF625b71)
val Pink40 = Color(0xFF7D5260)

val Grey = Color(0x80474747)
val Grey_transparent = Color(0x80000000)
val Grey_transparent_dark = Color(0xCC000000)
val Blue_dark = Color(0xFF0E5686)
val Blue_light = Color(0x802285C7)
val Gold = Color(0xFFCE8415)
val Green = Color(0xFF4D6B00)
val Red = Color(0xFF633836)