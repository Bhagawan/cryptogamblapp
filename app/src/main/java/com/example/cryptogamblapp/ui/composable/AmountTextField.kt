package com.example.cryptogamblapp.ui.composable

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Icon
import androidx.compose.material3.TextField
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.unit.dp
import com.example.cryptogamblapp.R
import com.example.cryptogamblapp.ui.theme.Green

@Composable
fun AmountTextField(modifier: Modifier = Modifier, onDone: (n: Int) -> Unit) {
    var amount by remember { mutableStateOf("") }
    Row(modifier = modifier, verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.Center) {
        TextField(value = amount,
            onValueChange = {
                try {
                    val newNumber = it.toInt()
                    amount = it
                } catch (e: Exception) {}
            },
            singleLine = true,
            modifier = Modifier.weight(1.0f, true).padding(0.0f.dp),
            keyboardActions = KeyboardActions(
                onDone = {
                    try {
                        val newNumber = amount.toInt()
                        onDone(newNumber)
                    } catch (e: Exception) {}
                    amount = ""
                }
            ),
            keyboardOptions = KeyboardOptions.Default.copy(keyboardType = KeyboardType.Number)
        )

        Button(onClick = {
            try {
                val newNumber = amount.toInt()
                onDone(newNumber)
            } catch (e: Exception) {}
            amount = ""
        }, shape = RoundedCornerShape(5.0.dp),
        colors = ButtonDefaults.buttonColors(containerColor = Green, contentColor = Color.White),
        modifier = Modifier.padding(5.0f.dp)) {
            Icon(painterResource(id = R.drawable.ic_baseline_check_24), contentDescription = stringResource(
                id = R.string.desc_add_money),
            modifier = Modifier.padding(5.0f.dp))
        }
    }
}