package com.example.cryptogamblapp.util

import android.graphics.Bitmap

object CurrentAppData {
    var back: Bitmap? = null
    var url = ""
    var totalBalance = 0

    var chosenCurrency = Currency.Bitcoin
}