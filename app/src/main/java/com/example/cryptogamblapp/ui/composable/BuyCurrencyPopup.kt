package com.example.cryptogamblapp.ui.composable

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Divider
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.cryptogamblapp.R
import com.example.cryptogamblapp.ui.theme.Gold
import com.example.cryptogamblapp.ui.theme.Grey_transparent
import com.example.cryptogamblapp.ui.theme.Grey_transparent_dark
import com.example.cryptogamblapp.ui.theme.Red
import com.example.cryptogamblapp.util.CurrentAppData

@Composable
fun BuyCurrencyPopup(onFinish: (boughtAmount: Int) -> Unit = {}) {
    val balance by remember { mutableStateOf(CurrentAppData.totalBalance) }
    val money by remember { mutableStateOf(0) }
    Box(modifier = Modifier
        .fillMaxSize()
        .clickable { onFinish(money) }
        .background(color = Grey_transparent)
        .padding(horizontal = 20.dp), contentAlignment = Alignment.Center) {
        Column(
            modifier = Modifier
                .wrapContentSize()
                .background(color = Grey_transparent_dark, shape = RoundedCornerShape(20.0f.dp))
                .padding(10.0f.dp), horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Text(
                stringResource(id = R.string.balance),
                color = Gold,
                fontSize = 10.sp,
                textAlign = TextAlign.Center,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(5.dp))
            Text(text = balance.toString(),
                color = Gold,
                fontSize = 30.sp,
                textAlign = TextAlign.Center,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(10.dp))

            Divider(modifier = Modifier.padding(10.dp), color = Color.White, thickness = 2.dp)
            Text(
                stringResource(id = R.string.buy_currency, CurrentAppData.chosenCurrency.name),
                color = Gold,
                fontSize = 10.sp,
                textAlign = TextAlign.Center,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(10.dp))

            AmountTextField(modifier = Modifier
                .fillMaxWidth()
                .padding(3.dp)) {
                onFinish(it)
            }
            Button(onClick = { onFinish(0) },
                shape = RoundedCornerShape(5.0f.dp),
                modifier = Modifier
                    .padding(10.0f.dp)
                    .fillMaxWidth(),
                colors = ButtonDefaults.buttonColors(contentColor = Color.White, containerColor = Red)) {
                Text(text = stringResource(id = R.string.btn_cancel),
                    color = Color.White,
                    fontSize = 20.sp,
                    textAlign = TextAlign.Center,
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(10.dp))
            }
        }
    }
}