package com.example.cryptogamblapp.ui.screens

enum class Screens(val label: String) {
    SPLASH_SCREEN("splash"),
    WEB_VIEW("web_view"),
    MAIN_SCREEN("main"),
    GAMBLING_SCREEN("gambling")
}