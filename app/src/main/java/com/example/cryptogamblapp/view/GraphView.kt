package com.example.cryptogamblapp.view

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Rect
import android.view.View
import com.example.cryptogamblapp.util.Coordinate
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlin.concurrent.fixedRateTimer
import kotlin.math.absoluteValue
import kotlin.math.atan
import kotlin.random.Random

class GraphView(context: Context): View(context){
    private var mWidth = 0
    private var mHeight = 0
    private var speed = 1.0f

    companion object {
        const val STATE_ACTIVE = 0
        const val STATE_PAUSE = 1
    }

    private val graph = ArrayList<Coordinate>()

    private var state = STATE_PAUSE

    private var mInterface: GraphInterface? = null

    init {
        fixedRateTimer("refresh", false, 0, 1000 / 60) {
            CoroutineScope(Dispatchers.Main).launch { invalidate() }
        }
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        if(w > 0) {
            mWidth = w - paddingStart - paddingEnd
            mHeight = h - paddingTop - paddingBottom
            speed = mWidth / 720.0f
            createInitialGraph()
            clipBounds = Rect(0,0,mWidth, mHeight)
        }
    }

    override fun onDraw(canvas: Canvas?) {
        canvas?.let {
            if(state == STATE_ACTIVE) updateGraph()
            drawGraph(it)
        }
    }

    /// Public

    fun setInterface(i: GraphInterface) {
        mInterface = i
    }

    fun start() {
        state = STATE_ACTIVE
    }

    fun pause() {
        state = STATE_PAUSE
    }

    /// Private

    private fun drawGraph(c: Canvas) {
        val p = Paint()
        p.color = Color.WHITE
        p.strokeWidth = 2.0f
        p.style = Paint.Style.STROKE
        c.drawRect(0.0f,0.0f,mWidth.toFloat(), mHeight.toFloat(), p)

        for(n in graph.indices) {
            if(n > 0 && graph[n].x <= mWidth / 2.0f) {
                c.drawLine(graph[n-1].x, graph[n-1].y, graph[n].x, graph[n].y, p)
            } else if(graph[n].x > mWidth / 2.0f) {
                val d = (graph[n].x - mWidth / 2.0f) / (graph[n].x - graph[n-1].x)
                val y = graph[n-1].y + (graph[n].y - graph[n-1].y) * (1 - d).absoluteValue
                c.drawLine(graph[n-1].x, graph[n-1].y, mWidth / 2.0f, y, p)
            }
        }
    }

    private fun updateGraph() {
        for(point in graph) {
            point.x-=speed
        }
        if(graph.size > 1) {
            if(graph[1].x <= 0) graph.removeAt(0)
        }
        if(graph.last().x <= mWidth / 2.0f) {
            spawnNewPoint()
            if(Random.nextInt(100) < 10) stop()
        }
    }

    private fun spawnNewPoint() {
        val diff = (1.0f - atan(Random.nextInt(5).toDouble()).absoluteValue / (Math.PI / 2)) * (mHeight / 2.0f)
        val lastX = if(graph.isNotEmpty()) graph.last().x else 0.0f
        val lastY = if(graph.isNotEmpty()) graph.last().y else mHeight / 2.0f
        val x = lastX + Random.nextInt(mWidth / 20)
        val y = Random.nextInt((lastY - diff).toInt().coerceAtLeast(0), (lastY + diff).toInt().coerceAtMost(mHeight))
        graph.add(Coordinate(x, y.toFloat()))
    }

    private fun stop() {
        state = STATE_PAUSE
        val diff = if(graph.size > 1) (graph[graph.size - 2].y - graph.last().y)  / (mHeight / 200.0f)
        else 0.0f
        mInterface?.onStop(diff)
    }

    private fun createInitialGraph() {
        do spawnNewPoint()
        while(graph.last().x < mWidth / 2.0f)
    }

    interface GraphInterface {
        fun onStop(diff: Float)
    }
}