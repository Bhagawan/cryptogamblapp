package com.example.cryptogamblapp.util

data class Coordinate(var x: Float, var y: Float)
