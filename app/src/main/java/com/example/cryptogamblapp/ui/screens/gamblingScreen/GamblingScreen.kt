package com.example.cryptogamblapp.ui.screens.gamblingScreen

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.viewinterop.AndroidView
import androidx.lifecycle.viewModelScope
import androidx.lifecycle.viewmodel.compose.viewModel
import coil.compose.rememberImagePainter
import com.example.cryptogamblapp.R
import com.example.cryptogamblapp.ui.composable.BuyCurrencyPopup
import com.example.cryptogamblapp.ui.composable.GamblResultPopup
import com.example.cryptogamblapp.ui.composable.GamblingActionComposable
import com.example.cryptogamblapp.ui.screens.Screens
import com.example.cryptogamblapp.ui.theme.Blue_dark
import com.example.cryptogamblapp.ui.theme.Green
import com.example.cryptogamblapp.ui.theme.Red
import com.example.cryptogamblapp.util.*
import com.example.cryptogamblapp.view.GraphView
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

@Preview
@Composable
fun GamblingScreen(currency: Currency = Currency.Bitcoin) {
    val viewModel = viewModel(GamblingScreenViewModel::class.java)
    val investedAmount = viewModel.investedAmount.collectAsState()
    val questionState = viewModel.question.collectAsState()
    val context = LocalContext.current

    Image(painter = rememberImagePainter(UrlBack), contentDescription = "", modifier = Modifier.fillMaxSize(), contentScale = ContentScale.FillBounds)
    Column(
        modifier = Modifier
            .fillMaxSize(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Row(
            Modifier
                .fillMaxWidth()
                .padding(10.0f.dp), verticalAlignment = Alignment.CenterVertically) {

            val currencyIcon = when(currency) {
                Currency.Bitcoin -> painterResource(id = R.drawable.ic_bitcoin_circle)
                Currency.Ethereum -> painterResource(id = R.drawable.ic_ethereum)
                Currency.Cronos -> painterResource(id = R.drawable.ic_cronos)
                Currency.Polygon -> painterResource(id = R.drawable.ic_polygon)
            }
            Icon(currencyIcon, contentDescription = "", Modifier.size(50.0f.dp))
            Text(
                text = investedAmount.value.toString(),
                fontSize = 30.0f.sp,
                color = Color.White,
                textAlign = TextAlign.Center,
                modifier = Modifier.weight(1.0f,true))

            Button(onClick = {
                viewModel.showPopup()
            }, shape = RoundedCornerShape(5.0.dp),
                colors = ButtonDefaults.buttonColors(containerColor = Green, contentColor = Color.White),
                modifier = Modifier.padding(10.0f.dp)) {
                Icon(painterResource(id = R.drawable.ic_baseline_add_24), contentDescription = stringResource(
                    id = R.string.desc_add_money),
                    modifier = Modifier.padding(5.0f.dp))
            }
        }
        Box(modifier = Modifier
            .fillMaxWidth()
            .fillMaxHeight(0.3f).padding(horizontal = 20.dp).background(Blue_dark)) {
            AndroidView(factory = { GraphView(it) },
                modifier = Modifier
                    .fillMaxSize(),
                update = { view ->
                    view.setInterface(object : GraphView.GraphInterface {
                        override fun onStop(diff: Float) {
                            viewModel.askQuestion(diff)
                        }
                    })
                    viewModel.graphActive.onEach {
                        if(it) view.start()
                        else view.pause()
                    }.launchIn(viewModel.viewModelScope)
                })
        }

        GamblingActionComposable(modifier = Modifier
            .fillMaxWidth()
            .weight(1.0f),
            if(viewModel.graphActive.replayCache.isNotEmpty()) viewModel.graphActive.replayCache.last() else true,
            questionState.value,
            viewModel::makeAGuess,
            viewModel::pauseGraph)

        TextButton(onClick = {
            CurrentAppData.totalBalance += investedAmount.value
            SavedPrefs.saveBalance(context)
            Navigator.navigateTo(Screens.MAIN_SCREEN)
        },
            modifier = Modifier
                .fillMaxWidth()
                .padding(10.dp),
            shape = RoundedCornerShape(20.dp),
            colors = ButtonDefaults.buttonColors(containerColor = Red,
                contentColor = Color.White)) {
            Text(stringResource(id = R.string.btn_take_money), textAlign = TextAlign.Center, color = Color.White, fontSize = 20.sp, modifier = Modifier.fillMaxWidth())
        }
    }

    val investPopup by viewModel.investPopup.collectAsState(false)
    val resultPopup by viewModel.result.collectAsState(false)

    if(investPopup) BuyCurrencyPopup {
        viewModel.buyCurrency(it)
        viewModel.hidePopup()
        SavedPrefs.saveBalance(context)
    }
    if(resultPopup != null) GamblResultPopup(resultPopup ?: true, amount = viewModel.getLastBalanceChange(), viewModel::hideResult)
}